# Kaggle New York taxi challenge
In this competition, Kaggle is challenging you to build a model that predicts the total ride duration of taxi trips in New York City. 
Your primary dataset is one released by the NYC Taxi and Limousine Commission, which includes pickup time, geo-coordinates, number of passengers, 
and several other variables. See more:
About the challenge: https://www.kaggle.com/c/nyc-taxi-trip-duration

## Generate extra data:
You can find the required files over the given train/test data here:
https://www.kaggle.com/oscarleo/new-york-city-taxi-with-osrm/data

UPDATE: The extended data source is not available anymore. You can find the raw for training in: ```data/data.zip```

* fastest_routes_train_part_1.csv
* fastest_routes_train_part_2.csv
* fastest_routes_test.csv
* accidents_2016.csv

I think at the beginning, faster_routes_train_part1 and part2 should be merged. Just run:
```bash
# to remove the first comment line
tail -n +2 fastest_routes_train_part_1.csv > fastest_routes_train_part_1.csv_temp 
tail -n +2 fastest_routes_train_part_2.csv > fastest_routes_train_part_2.csv_temp

mv fastest_routes_train_part_1.csv_temp  fastest_routes_train.csv
cat fastest_routes_train_part_2.csv_temp >> fastest_routes_train.csv

#same for train.csv
tail -n +2 train.csv > train.csv_tmp
mv train.csv_tmp train.csv
```

Then we should sort the *fastest_routes_train.csv* for the more efficient searching later. Please run:
```bash
python trip_sorting.py
```

After finishing the script (it may take 5-10min), you should get *fastest_routes_train_sorted.csv*. Later it will be used for making extended training data sheet. In order to generate the extended datasheet
please run:
```bash
python data_transform.py
```
Note: one data line has no *fastes_trip*, so it is just simply discarded. 

## Training data:
* trip_id (e.g: id2875421)
* vendor_id (e.g: 2)
* pickup_datetime (e.g: 2016-03-14 17:24:55)
* dropoff_datetime (e.g: 2016-03-14 17:32:30)
* passenger_count (e.g: 2)
* pickup_longitude (e.g: -73.982154846191406)
* pickup_latitude  (e.g: 40.767936706542969)
* dropoff_longitude (e.g: -73.964630126953125)
* dropoff_latitude (e.g: 40.765602111816406)
* store_and_fwd_flag (e.g: N)
* trip_duration (e.g: 455)
* total_distance (e.g: 2009.1)
* estimated_time (e.g: 164.9)
* number_of_steps (e.g: 5)
* accident_delta_time (e.g: 30055)
* accident_distance (e.g: 0.16288987184920065)
* accident_vehicles [1, 0, 0, 0, 0]
