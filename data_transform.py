#!/usr/bin/python3
import csv
from enum import Enum
import numpy as np
import os
import datetime
import math
import bisect
ACCIDENTS_FILE = "data/accidents_2016.csv"
ACCIDENTS_LEN = os.stat(ACCIDENTS_FILE).st_size
FASTEST_TRIPS_FILE_SORTED = "data/fastest_routes_train_sorted.csv"
TRAIN_DATA_FILE = "data/train.csv"
TRAIN_DATA_EXTENDED = "data/train_extended.csv"

class Input(Enum):
    id = 0
    vendor_id = 1
    pickup_datetime = 2
    dropoff_datetime = 3
    passenger_count = 4
    pickup_longitude = 5
    pickup_latitude = 6
    dropoff_longitude = 7
    dropoff_latitude = 8
    store_and_fwd_flag = 9
    trip_duration = 10


class AccidentInput(Enum):
    latitude = 2
    longitude = 3
    person_killed = 8
    person_injured = 9
    pedestrian_killed = 10
    pedestrian_injured = 11
    cyclist_killed = 12
    cyclist_injured = 13
    motorist_killed = 14
    motorist_injured = 15
    vehicle_1 = 22
    vehicle_2 = 23
    vehicle_3 = 24
    vehicle_4 = 25
    vehicle_5 = 26
    date = 27


class FastestRouteInput(Enum):
    id = 0
    starting_street = 1
    end_street = 2
    total_distance = 3
    total_travel_time = 4
    number_of_steps = 5
    street_for_each_step = 6
    distance_per_step = 7
    travel_time_per_step = 8
    step_maneuvers = 9
    step_direction = 10
    step_location_list = 11


class ExtraData(Enum):
    accident_delta_time = 0
    accident_distance = 1


class AccidentContributors(Enum):
    none = -1
    car = 0
    pedestrian = 1
    bicycle = 2
    motorcycle = 3
    other = 4


def date_transform(date1):

    """
    :param date parameter in "2016-04-05 14:44:25" format
    :return date as dateformat
    """
    date1 = date1.split(" ")
    date1_date = date1[0].split("-")
    date1_time = date1[1].split(":")

    date1_dateformat = datetime.datetime(int(date1_date[0]), int(date1_date[1]), int(date1_date[2]),
                                         int(date1_time[0]), int(date1_time[1]), int(date1_time[2]))
    return date1_dateformat


def accident_time_compare(trip_time, accident_time):

    """
    :param  trip_time datetime trip start
    :param  accident_time datetime of accident
    :return True if accident happened 1 hour before trip start, False if not
    """

    date_diff = trip_time - accident_time
    if 0 < date_diff.total_seconds() < datetime.timedelta(hours=1).total_seconds():
        return True
    else:
        return False


def get_accident_contrib(vehicle):
    """
    :param vehicle string
    :param AccidentContributors
    """

    if ("VAN" in vehicle) or ("PASSENGER VEHICLE" in vehicle):
        return AccidentContributors.car.value
    elif "MOTORCYCLE" in vehicle:
        return AccidentContributors.motorcycle.value
    elif "BICYCLE" in vehicle:
        return AccidentContributors.bicycle.value
    elif vehicle == "":
        return AccidentContributors.none.value
    else:
        return AccidentContributors.other.value


def get_accident_vehicles(accident_line):
    """
    :param accident_line: csv input of accidents
    :return: array with boolean value of contributors e.g.: [1 0 1 0 0]
    """

    accident_vehicles = np.zeros(AccidentInput.vehicle_5.value - AccidentInput.vehicle_1.value + 1)
    for i in range(len(accident_vehicles)):
        vehicle = get_accident_contrib(accident_line[AccidentInput.vehicle_1.value + i])
        if vehicle != AccidentContributors.none.value:
            accident_vehicles[vehicle] = 1

    return accident_vehicles


def get_accident_data(input_line):
    """
    :param input_line: data line from accidents csv file
    :return: delta time from departure in seconds, [coordinates x, y], accident vehicles array
    """

    trip_start_date = date_transform(input_line[Input.pickup_datetime.value])

    #get soonest accident
    f = open(ACCIDENTS_FILE)
    csv_reader = csv.reader(f, delimiter=",")
    time_interval = 0
    #accident_moving_window = np.chararray((5, AccidentInput.date.value + 1))
    for acc_line in csv_reader:
        accident_date = date_transform(acc_line[AccidentInput.date.value])
        delta_time = trip_start_date - accident_date
        if time_interval == 0 or delta_time < time_interval:
            time_interval = delta_time
        else:
            break
    f.close()

    return [delta_time.seconds, [acc_line[AccidentInput.longitude.value], acc_line[AccidentInput.latitude.value]], get_accident_vehicles(acc_line)]


def get_shortest_trip_data(fastes_trip_list, fastes_trip_id_list, trip_line):
    """
    :param fastes_trip_list: list of fastest trip (the whole list, pretty big)
    :param fastes_trip_id_list: list of indexes as integers, for faster lookup
    :param trip_line: line of data from original csv file
    :returns location list of steps, total_distance, estimated_time, number_of_steps
    """
    trip_id = int(trip_line[0][2:])
    index = bisect.bisect_left(fastes_trip_id_list, trip_id)
    if fastes_trip_list[index][0] != trip_line[0]:
        raise ValueError
    return fastes_trip_list[index][FastestRouteInput.step_location_list.value], \
           fastes_trip_list[index][FastestRouteInput.total_distance.value], \
           fastes_trip_list[index][FastestRouteInput.total_travel_time.value], \
           fastes_trip_list[index][FastestRouteInput.number_of_steps.value]


def parse_step_coordinates(steps_string):

    steps_array1 = steps_string.split("|")
    steps_array2 = np.zeros((len(steps_array1), 2))
    for i in range(len(steps_array1)):
        steps_array2[i][0] = float(steps_array1[i].split(",")[0])
        steps_array2[i][1] = float(steps_array1[i].split(",")[1])

    return steps_array2


def get_distance_from_accident(route_steps, accident):
    """
    :param route_steps: route steps as numpy array
    :param accident:    accident coordinates as array
    :return: relative distance from the closest point
    """
    min_distance = 0

    for i in range(1, len(route_steps)):
        distance = math.sqrt(((route_steps[i][0] - float(accident[0])) ** 2) + ((route_steps[i][1] - float(accident[1])) ** 2))
        if min_distance == 0 or distance < min_distance:
            min_distance = distance

    return distance


if __name__ == "__main__":

    f = open(FASTEST_TRIPS_FILE_SORTED, 'r')
    csv_reader = csv.reader(f, delimiter=",")
    fastes_trip_list = []
    fastes_trip_id_list = []
    for line in csv_reader:
        fastes_trip_list.append(line)
        fastes_trip_id_list.append(int(line[0][2:]))
    f.close()

    f = open(TRAIN_DATA_FILE, 'r')
    fw = open(TRAIN_DATA_EXTENDED, 'w')
    csv_reader = csv.reader(f, delimiter=",")
    csv_writer = csv.writer(fw, delimiter=",")
    i = 0
    for line in csv_reader:
        i += 1
        # get accident data
        accident_delta_time, accident_coordinates, accident_vehicles = get_accident_data(line)

        #get shortest trip data
        try:
            location_list, total_distance, estimated_time, number_of_steps = get_shortest_trip_data(fastes_trip_list, fastes_trip_id_list, line)
        except ValueError:
            continue
        accident_distance = get_distance_from_accident(
            parse_step_coordinates(location_list),
            [accident_coordinates[0],
             accident_coordinates[1]])

        if i % 1000 == 0:
            print(i, datetime.datetime.now())

        line.extend([total_distance, estimated_time, number_of_steps, accident_delta_time, accident_distance])
        line.extend(accident_vehicles)
        csv_writer.writerows([line])

    fw.close()
