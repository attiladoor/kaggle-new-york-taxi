#!/usr/bin/python3
import csv
import numpy as np
from enum import Enum
import matplotlib.pyplot as plt
import datetime

class input(Enum):
    id = 0
    vendor_id = 1
    pickup_datetime = 2
    dropoff_datetime = 3
    passenger_count = 4
    pickup_longitude = 5
    pickup_latitude = 6
    dropoff_longitude = 7
    dropoff_latitude = 8
    store_and_fwd_flag = 9
    trip_duration = 10

def autolabel(rects, ax):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2., height,
                '%d' % int(height),
                ha='center', va='bottom')


""" @brief  reveals number of taxi vendors from data file
    @return number of vendors as integer
"""

def get_number_of_vendors():
    f = open("data/train.csv")
    csv_reader = csv.reader(f, delimiter=",")

    vendors = []
    for line in csv_reader:
        if line[input.vendor_id.value] not in vendors:
            try:
                # checking if value is an integer
                int(line[input.vendor_id.value])
                vendors.append(line[input.vendor_id.value])
            except ValueError:
                pass

    return len(vendors)


def get_average_time_by_vendor():
    """
        plot average trip duration time regarding to taxi vendor
    """
    vendors_num = get_number_of_vendors()
    f = open("data/train.csv")
    csv_reader = csv.reader(f, delimiter=",")

    vendor_time = np.zeros(vendors_num)
    vendor_trips = np.zeros(vendors_num)

    for line in csv_reader:
        try:
            vendor_time[int(line[input.vendor_id.value]) - 1] += int(line[input.trip_duration.value])
            vendor_trips[int(line[input.vendor_id.value]) - 1] += 1
        except ValueError:
            pass

    ind = np.arange(vendors_num)  # the x locations for the groups
    fig, ax = plt.subplots()

    # get averages
    for i in range(vendors_num):
        vendor_time[i] /= vendor_trips[i]

    rects1 = ax.bar(ind, vendor_time, color='r')
    ax.set_ylabel('Average trip duration (s)')
    ax.set_title('Average trip duration by vendor')
    ax.set_xticks(ind)
    ax.set_xticklabels(('Vendor1', 'Vendor2'))

    autolabel(rects1, ax)
    plt.show()


def get_day_by_date(date):
    """
    :param date: input date in 2016-03-14 17:24:55
    :return: day of the week as integer, monday = 0, sunday = 6
    """
    date = date.split(" ")
    date = date[0].split("-")
    try:
        return datetime.datetime(int(date[0]), int(date[1]), int(date[2])).weekday()
    except ValueError:
        return 0


def get_average_time_by_day():
    """
        plot average trip duration by day of the week
    """
    N = 7
    time = np.zeros(N)
    number = np.zeros(N)
    f = open("data/train.csv")
    csv_reader = csv.reader(f, delimiter=",")

    for line in csv_reader:
        try:
            day = get_day_by_date(line[input.pickup_datetime.value])
            time[day] += int(line[input.trip_duration.value])
            number[day] += 1
        except TypeError:
            pass
        except ValueError:
            pass

    for i in range(N):
        time[i] /= number[i]

    ind = np.arange(N)  # the x locations for the groups
    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, time, color='r')
    ax.set_ylabel('Average trip duration (s)')
    ax.set_title('Average trip duration by days')
    ax.set_xticks(ind)
    ax.set_xticklabels(('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'))

    autolabel(rects1, ax)
    plt.show()


def get_average_time_by_hour():
    """
        plot average trip duration by day of the week
    """
    N = 24
    time = np.zeros(N)
    number = np.zeros(N)
    f = open("data/train.csv")
    csv_reader = csv.reader(f, delimiter=",")

    for line in csv_reader:
        if line[input.id.value] != 'id':
            hour = line[input.pickup_datetime.value].split(" ")
            hour = hour[1].split(":")
            time[int(hour[0])] += int(line[input.trip_duration.value])
            number[int(hour[0])] += 1

    for i in range(N):
        time[i] /= number[i]

    ind = np.arange(N)  # the x locations for the groups
    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, time, color='r')
    ax.set_ylabel('Average trip duration (s)')
    ax.set_title('Average trip duration by hours')
    ax.set_xticks(ind)

    ax.set_xticklabels(range(1, N + 1))

    autolabel(rects1, ax)
    plt.show()

get_average_time_by_hour()
