import unittest
import data_transform
import datetime
import numpy as np


class TestDataTransformDateTransform(unittest.TestCase):

    def test(self):
        date1 = data_transform.date_transform("2016-04-05 14:44:25")
        date2 = datetime.datetime(2016, 4, 5, 14, 44, 25)
        self.assertEqual(date1, date2)


class TestAccidentTimeCompare(unittest.TestCase):

    def test1(self):
        trip = datetime.datetime(2016, 4, 1, 14, 44, 25)
        accident = datetime.datetime(2016, 4, 1, 14, 34, 25)
        self.assertTrue(data_transform.accident_time_compare(trip, accident))

    def test2(self):
        trip = datetime.datetime(2016, 4, 1, 14, 44, 25)
        accident = datetime.datetime(2016, 4, 1, 15, 45, 25)
        self.assertFalse(data_transform.accident_time_compare(trip, accident))

    def test3(self):
        trip = datetime.datetime(2016, 4, 1, 14, 44, 25)
        accident = datetime.datetime(2016, 3, 1, 15, 45, 25)
        self.assertFalse(data_transform.accident_time_compare(trip, accident))


class TestParseStepCoordinates(unittest.TestCase):

    def test1(self):
        result = data_transform.parse_step_coordinates("-73.981372,40.752814|-73.98318,40.750341|-73.989243,40.752895|-73.992006,40.749101|-73.993845,40.749874")
        array = [[-73.981372, 40.752814], [-73.98318, 40.750341], [-73.989243, 40.752895], [-73.992006, 40.749101], [-73.993845, 40.749874]]
        self.assertTrue(np.array_equal(result, array))


class TestGetDistanceFromAccident(unittest.TestCase):

    def test1(self):
        steps = [[-73.981372, 40.752814], [-73.98318, 40.750341], [-73.989243, 40.752895], [-73.992006, 40.749101],
                 [-73.993845, 40.749874]]
        accident = [-73.993842, 40.749879]
        self.assertEqual(data_transform.get_distance_from_accident(steps, accident), 0.00000583095189230927)


class TestGetAccidentContributor(unittest.TestCase):

    def test1(self):
        self.assertEqual(data_transform.get_accident_contrib("PASSENGER VEHICLE"), data_transform.AccidentContributors.car.value)

    def test2(self):
        self.assertEqual(data_transform.get_accident_contrib("VAN"),
                         data_transform.AccidentContributors.car.value)

    def test3(self):
        self.assertEqual(data_transform.get_accident_contrib("BICYCLE"), data_transform.AccidentContributors.bicycle.value)

    def test4(self):
        self.assertEqual(data_transform.get_accident_contrib("MOTORCYCLE"), data_transform.AccidentContributors.motorcycle.value)

    def test5(self):
        self.assertEqual(data_transform.get_accident_contrib("FOO"), data_transform.AccidentContributors.other.value)

    def test6(self):
        self.assertEqual(data_transform.get_accident_contrib(""),
                         data_transform.AccidentContributors.none.value)


class TestGetAccidentVehicles(unittest.TestCase):

    def test1(self):
        self.assertTrue(np.array_equal(
            data_transform.get_accident_vehicles(",,,,,,,,1.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,Unspecified,,,,,3448403.0,PASSENGER VEHICLE,,,,,2016-05-27 17:30:00".split(",")),
                [1, 0, 0, 0, 0]))

    def test2(self):
        self.assertTrue(np.array_equal(
            data_transform.get_accident_vehicles("QUEENS,11433.0,,,,brewer boulevard,111th avenue,,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,Unspecified,Unspecified,,,,3458894.0,PASSENGER VEHICLE,PASSENGER VEHICLE,,,,2016-06-09 11:30:00".split(",")),
                [1, 0, 0, 0, 0]))

    def test3(self):
        self.assertTrue(np.array_equal(
            data_transform.get_accident_vehicles(",,,,,,,,1.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,Unspecified,,,,,3448403.0,PASSENGER VEHICLE,MOTORCYCLE,,,,2016-05-27 17:30:00".split(",")),
                [1, 0, 0, 1, 0]))

    def test4(self):
        self.assertTrue(np.array_equal(
            data_transform.get_accident_vehicles(",,,,,,,,1.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,Unspecified,,,,,3448403.0,PASSENGER VEHICLE,VAN,,,,2016-05-27 17:30:00".split(",")),
                [1, 0, 0, 0, 0]))

if __name__ == '__main__':
    unittest.main()

