import csv
import datetime
import bisect

FASTEST_TRIPS_FILE = "data/fastest_routes_train.csv"
FASTEST_TRIPS_FILE_OUT = "data/fastest_routes_train_sorted.csv"

if __name__ == "__main__":

    all_line_iterator = 0
    csv_array = []
    id_lookup_array = []

    f = open(FASTEST_TRIPS_FILE, 'r')
    csv_reader = csv.reader(f, delimiter=",")
    i = 0
    for line in csv_reader:
        line_id = int(line[0][2:])
        new_id = bisect.bisect_left(id_lookup_array, line_id)
        id_lookup_array.insert(new_id, line_id)
        csv_array.insert(new_id, line)

        i += 1
        if i % 1000 == 0:
            print(i, datetime.datetime.now())

    f.close()

    #writing to file
    f = open(FASTEST_TRIPS_FILE_OUT, 'w')
    csv_writer = csv.writer(f, delimiter=",", lineterminator='\n')

    for i in range(len(csv_array)):
        csv_writer.writerows([csv_array[i]])
    f.close()

    #TEST
    f = open(FASTEST_TRIPS_FILE_OUT, 'r')
    csv_reader = csv.reader(f, delimiter=",")
    old_id = 0
    for line in csv_reader:
        line_id = int(line[0][2:])
        if old_id > line_id:
            print("UNSORTED!")
            exit(1)
        old_id = line_id

    print("SORTED!")
